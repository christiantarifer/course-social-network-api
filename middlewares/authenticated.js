'use strict'

// * LOAD LIBRARIES

let jwt = require('jwt-simple')
let moment = require('moment')

// * SECRET
const secret = 'clave_secreta_curso_desarrollar_red_social_angular';

exports.ensureAuth = function (req, res, next){

    let authorization = req.headers.authorization

    if(!authorization){
        return res.status(403).send({
            message: 'The request does not have the authentication header.'
        })
    }

    // * REMOVE QUOTATION FROM THE AUTHORIZATION
    let token = authorization.replace(/['"]+/g, '')

    let payload = ''

    try {
        payload = jwt.decode(token, secret);

        if (payload.exp <= moment().unix() ){

            return res.status(401).send({
                message: 'Token has expired'
            })

        }

    } catch (error) {
        return res.status(404).send({
            message: 'Token is invalid'
        })
    }

    req.user = payload

    next()
}