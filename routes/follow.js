'use strict';

// * PACKAGES

const express = require('express');

// * CONTROLLERS

const FollowController = require('../controllers/follow');

let api = express.Router();


// * MIDDLEWARES
let md_auth = require('../middlewares/authenticated');

// * ROUTES
// - ********************************************************** - //

api.get('/pruebas-follow', md_auth.ensureAuth, FollowController.prueba);

// - ********************************************************** - //

api.post('/follow', md_auth.ensureAuth, FollowController.saveFollow);

// - ********************************************************** - //

api.delete('/follow/:id', md_auth.ensureAuth, FollowController.deleteFollow);

// - ********************************************************** - //

api.get('/following/:id?/:page?', md_auth.ensureAuth, FollowController.getFollowingUser);

// - ********************************************************** - //

api.get('/followed/:id?/:page?', md_auth.ensureAuth, FollowController.getFollowedUsers);

// - ********************************************************** - //

api.get('/get-my-follows/:followed?', md_auth.ensureAuth, FollowController.getMyFollows);

// - ********************************************************** - //
module.exports = api;
