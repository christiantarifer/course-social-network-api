'use strict';

//  * PACKAGES
const express = require('express');
const MessageController = require('../controllers/message');

// * EXPRESS ROUTER
const api = express.Router();

// * MIDDLEWARES
const md_audth = require('../middlewares/authenticated');

// * ROUTES

api.get('/probando-md', md_audth.ensureAuth, MessageController.probando);
api.post('/message', md_audth.ensureAuth, MessageController.saveMessage);
api.get('/my-messages/:page?', md_audth.ensureAuth, MessageController.getReceivedMessages);
api.get('/messages/:page?', md_audth.ensureAuth, MessageController.getEmittedMessages);
api.get('/unviewed-messages', md_audth.ensureAuth, MessageController.getUnviewedMessages);
api.get('/set-viewed-messages', md_audth.ensureAuth, MessageController.setViewedMessages);

module.exports = api;