'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const app = express();

// * LOAD ROUTES
const user_routes = require('./routes/user');
const follows_routes = require('./routes/follow');
const publications_routes = require('./routes/publication');
const messages_routes = require('./routes/message');

// * MIDDLEWARES
app.use( bodyParser.urlencoded({ extended: false }) );
app.use( bodyParser.json() );

// * CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
 
    next();
});


// * ROUTES
app.use('/api', user_routes);
app.use('/api', follows_routes);
app.use('/api', publications_routes);
app.use('/api', messages_routes);

// * Export
module.exports = app;