'use strict';

// * PACKAGES TO USE
let path = require('path');

let fs = require('fs');

let moment = require('moment');

let mongoosePagination = require('mongoose-pagination');

// * PUBLICATIONS

let Publication = require('../models/publication');
let User = require('../models/user');
let Follow = require('../models/follow');

// | -------------------------------------------------------------------------------------------------------------- | //

function probando(req, res) {
    res.status(200).send( {message: 'Hello world from Publications controller'})
}

// | -------------------------------------------------------------------------------------------------------------- | //

function savePublication(req, res){

    let params = req.body;

    if ( !params.text ) return res.status(200).send({ message: 'Debes enviar un texto' });

    let publication = new Publication();

    publication.text = params.text;
    publication.file = null;
    publication.user = req.user.sub;
    publication.created_at = moment().unix();

    publication.save( (err, publicationStored) => {

        if ( err ) return res.status(500),send({ message: 'There was an error on the publciation process.' });

        if ( !publicationStored ) return res.status(404).send({ message: 'The publication was not set' });

        return res.status(200).send({ publication: publicationStored });


    })


}

// | -------------------------------------------------------------------------------------------------------------- | //

function getPublications(req, res) {

    let page = 1;

    if ( req.params.page ) {

        page = req.params.page;

    }

    let itemsPerPage = 4;

    Follow.find({ user: req.user.sub }).populate('followed').exec( (err, follows) => {
    
        if ( err ) return res.status(500),send({ message: 'There was an error while retrieving the follow.' });

        let follows_clean = [];

        follows.forEach( (follow) => {

            follows_clean.push(follow.followed);

        });

        // ADD OWN USER'S ID
        follows_clean.push( req.user.sub );

        Publication.find({ user: {"$in": follows_clean} }).sort('-created_at').populate('user').paginate(page, itemsPerPage, (err, publications, total) => {

            if ( err ) return res.status(500),send({ message: 'There was an error while retrieving the post.' });

            if ( !publications ) return res.status(404).send({ message: 'There are no post.' });

            return res.status(200).send({
                total_items: total,
                pages: Math.ceil(total/itemsPerPage),
                page: page,
                items_per_page: itemsPerPage,
                publications
            })

        });



    })

}

// | -------------------------------------------------------------------------------------------------------------- | //

function getPublicationsUser(req, res) {

    let page = 1;

    if ( req.params.page ) {

        page = req.params.page;

    }

    let user = req.user.sub;

    if( req.params.user ) {

        user = req.params.user;

    }

    let itemsPerPage = 4;

    Publication.find({ user: user }).sort('-created_at').populate('user').paginate(page, itemsPerPage, (err, publications, total) => {

        if ( err ) return res.status(500),send({ message: 'There was an error while retrieving the post.' });

        if ( !publications ) return res.status(404).send({ message: 'There are no post.' });

        return res.status(200).send({
            total_items: total,
            pages: Math.ceil(total/itemsPerPage),
            page: page,
            items_per_page: itemsPerPage,
            publications
        })

    });




}

// | -------------------------------------------------------------------------------------------------------------- | //

function getPublication(req, res) {

    let publicationId = req.params.id;

    Publication.findById(publicationId, (err, publication) => {

        if ( err ) return res.status(500).send({ message: 'There was an error while retrieving the post.' });

        if ( !publication ) return res.status(404).send({ message: 'There ara no post' });

        res.status(200).send({publication});

    })

}

// | -------------------------------------------------------------------------------------------------------------- | //

function deletePublication(req, res) {

    let publicationId = req.params.id;

    Publication.find({'user': req.user.sub, '_id':publicationId}).deleteOne((err, publicationDeleted) => {

        if(err) return res.status(500).send({ message: 'The post has no been deleted.' });

        if( !publicationDeleted ) return res.status(404).send({ message: 'The post has not been deleted.' });

        return res.status(200).send({ publication: publicationDeleted })

    })

}

// | *********************************************************** | //

// * UPLOAD AND POST IMAGE
function uploadImage(req, res) {

    const publicationId = req.params.id

    console.group
    console.log(req.files);
    console.log(req.files.image);
    console.groupEnd
    

    const size = req.files.image.size
    const type = req.files.image.type
    const file_path = req.files.image.path;

    console.log(size)

    if ( size > 0) {

        const file_split = file_path.split('\/');

        const file_name = file_split[2]

        const ext_split = file_name.split('\.')

        const file_ext = ext_split[1];


        if ( file_ext === 'png' || file_ext === 'jpg' || file_ext === 'jpeg' || file_ext === 'gif' ) {

            Publication.findOne({ 'user': req.user.sub, '_id': publicationId }).exec( (err, publication) => {

                if( publication ) {

                    Publication.findByIdAndUpdate(publicationId, {file: file_name}, {new: true}, (err, publicationUpdated) => {

                        if(err) return res.status(500).send({ message: 'There was an error on the request.' })
        
                        if(!publicationUpdated) return res.status(404).send({ message: 'There is not publication to update.' })
        
                        return res.status(200).send({ publication: publicationUpdated });
        
                    })

                } else {

                    return removeFilesOfUploads(res, file_path, 'You do not have permissions to change the image.')

                }

            })

            
            
        } else {

            return removeFilesOfUploads(res, file_path, 'Extention is invalid')

        }


    } else {

        return removeFilesOfUploads(res, file_path, 'The file was not uploaded')

    }

}

// | *********************************************************** | //

function removeFilesOfUploads(res, file_path, message) {
    fs.unlink(file_path, (err) => {
        console.log(err);
        return res.status(404).send({message: message});
    });
}
    


// | *********************************************************** | //

function getImageFile ( req, res ) {

    const image_file = req.params.imageFile

    console.log(image_file);

    const path_file = `./uploads/publications/${image_file}`

    fs.exists(path_file, (exists) => {

        if(exists) {

            res.sendFile(path.resolve(path_file));

        } else {

            res.status(200).send({ message: 'Image does not exists.'})

        }


    })

}


// | -------------------------------------------------------------------------------------------------------------- | //

function getCounters (req, res) {

    // * GET CLIENT'S ID
    let userId = req.user.sub;

    // * CLIENT'S ID BY THE URL
    if( req.params.id ) {

        userId = req.params.id;

    }

    getCountFollow( userId ).then( (value) => {

        return res.status(200).send( value );

    })

}

async function getCountFollow(user_id) {

    let following = await Follow.countDocuments(( { "user" : user_id } )).exec().then( (count) => {

                                        return count;

                                    }).catch( (err) => {

                                        return handleError(err);

                                    })

    let followed = await Follow.countDocuments(( { "followed" : user_id } )).exec().then( (count) => {

                                        return count;

                                    }).catch( (err) => {

                                        return handleError(err);

                                    })

    return {
        following: following,
        followed: followed
    }

}

// | -------------------------------------------------------------------------------------------------------------- | //

module.exports = {
    probando,
    savePublication,
    getPublications,
    getPublicationsUser,
    getPublication,
    deletePublication,
    uploadImage,
    getImageFile,
}