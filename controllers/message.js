'use strict';

// * PACKAGES
const moment = require('moment');
const mongoosePaginate = require('mongoose-pagination');

//  * MODELS
const User = require('../models/user');
const Follow = require('../models/follow');
const Message = require('../models/message');


// | * ------------------------------------------------------------------------------------------------------------------ * | //

function probando (req, res) {
    res.status(200).send({ message: 'This is the endpoint of messages' });
}

// | * ------------------------------------------------------------------------------------------------------------------ * | //

function saveMessage (req, res) {

    let params = req.body;

    let text = params.text;
    let emitter = req.user.sub;
    let receiver = params.receiver;

    if ( !text || !receiver ) return res.status(200).send({ message: 'Send the required data.' });

    let message = new Message();

    message.emitter = emitter;

    message.receiver = receiver;

    message.text = text;

    message.created_at = moment().unix();

    message.viewed = 'false';

    message.save( (err, messageStored) => {

        if ( err ) return res.status(500).send({ message: `There is an error on the request: ${err}` });

        if ( !messageStored ) return res.status(404).send({ message: 'There was an error when saving the message' });

        return res.status(200).send({ message: messageStored });

    });
}

// | * ------------------------------------------------------------------------------------------------------------------ * | //

function getReceivedMessages (req, res) {

    let userId = req.user.sub;

    let page = 1;

    if ( req.params.page ) {

        page = req.params.page;

    }

    let itemsPerPage = 4;

    Message.find({ receiver: userId }).populate('emitter', 'name surname image nick _id').sort('-created_at').paginate(page, itemsPerPage, (err, messages, total) => {

        if ( err ) return res.status(500).send({ message: `There is an error on the request: ${err}` });

        if ( !messages ) return res.status(404).send({ message: 'There are no messages no show.' });

        return res.status(200).send({ 
            total: total,
            pages: Math.ceil(total/itemsPerPage),
            messages
         });

    });

}

// | * ------------------------------------------------------------------------------------------------------------------ * | //

function getEmittedMessages(req, res) {

    let userId = req.user.sub;

    let page = 1;

    if ( req.params.page ) {

        page = req.params.page;

    }

    let itemsPerPage = 4;

    Message.find({ emitter: userId }).populate('emitter receiver', 'name surname image nick _id').paginate(page, itemsPerPage, (err, messages, total) => {

        if ( err ) return res.status(500).send({ message: `There is an error on the request: ${err}` });

        if ( !messages ) return res.status(404).send({ message: 'There are no messages no show.' });

        return res.status(200).send({ 
            total: total,
            pages: Math.ceil(total/itemsPerPage),
            messages
         });

    });

}

// | * ------------------------------------------------------------------------------------------------------------------ * | //

function getUnviewedMessages(req, res) {

    let userId = req.user.sub;

    Message.countDocuments({ receiver: userId, viewed: 'false' }).exec( (err, count) => {

        if ( err ) return res.status(500).send({ message: `There is an error on the request: ${err}` });

        return res.status(200).send({ 

            'unviewed': count

         })


    });

}

// | * ------------------------------------------------------------------------------------------------------------------ * | //

function setViewedMessages(req, res) {

    let userId = req.user.sub;

    Message.update( { receiver: userId, viewed: 'false' }, {viewed: 'true'}, { "multi" : true}, (err, messageUpdated) =>  {

        if ( err ) return res.status(500).send({ message: `There is an error on the request: ${err}` });

        if ( !messageUpdated ) return res.status(404).send({ message: 'The messages could not be updated' });

        return res.status(200).send({ messages: messageUpdated });

    })

}

// | * ------------------------------------------------------------------------------------------------------------------ * | //
module.exports = {
    probando,
    saveMessage,
    getReceivedMessages,
    getEmittedMessages,
    getUnviewedMessages,
    setViewedMessages,
}