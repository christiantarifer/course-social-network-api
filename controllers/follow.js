'use strict';

// const path = require('path');

// const fs = require('fs');

const mongoosePaginate = require('mongoose-pagination');



// * LOAD MODELS

const User = require('../models/user');

const Follow = require('../models/follow');

// * ENDPOINTS

// - ******************************************************************************** - //

function prueba(req, res){

    res.status(200).send({ message: 'Hello world from Follows controller.' });

}

// - ******************************************************************************** - //

function saveFollow(req, res){

    // * GET DATA FROM THE BODY
    const params = req.body;

    let client = req.user.sub;

    let follower = params.followed;

    if (client === follower ) {

        return res.status(404).send({ message: 'The client cannot follow it\'s own account.' })

    }

    let follow = new Follow();

    follow.user = client;
    follow.followed = follower;



    follow.save( (err, followStored) => {

        if ( err ) return res.status(500).send({ message: `There was an error at following the user: ${err}.` });

        if ( !followStored ) return res.status(404).send({ message: 'The following has no been stored' });

        return res.status(200).send({ follow: followStored })

    })

}

// - ******************************************************************************** - //

function deleteFollow(req, res) {

    // * CURRENT LOGGED USER
    const userId = req.user.sub;

    // * FOLLOWED USER
    const followId = req.params.id;


    Follow.find({ 'user':userId, 'followed':followId }).deleteOne(err => {

        if( err ) return res.status(500).send({ message: 'There was an error on the unfollow process.' });

        return res.status(200).send({ message: 'The follow has been deleted.' });

    })
}

// - ******************************************************************************** - //

function getFollowingUser(req, res){

    // * CURRENT LOGGED USER
    let userId = req.user.sub;

    if( req.params.id && req.params.page ){

        userId = req.params.id;

    }

    let page = 1;

    if( req.params.page ) {

        page = req.params.page;

    } else {

        page = req.params.id;

    }

    let itemsPerPage = 4;

    Follow.find( { user: userId }).populate({ path: 'followed' }).paginate(page, itemsPerPage, (err, follows, total) => {

        if( err ) return res.status(500).send({ message: 'There was an error on the following list.' });

        if( !follows ) return res.status(404).send({ message: 'You are not following any user.' });

        followUserIds( userId ).then( (value) => {

            return res.status(200).send({
                total: total,
                pages: Math.ceil(total/itemsPerPage),
                follows,
                users_following: value.following,
                users_follow_me: value.followed,
            })

        })
        
    });
}

async function followUserIds(userId) {

    let following = await Follow.find({ "user": userId }).select({ '_id': 0, '__v': 0, 'user': 0 }).exec().then( (follows) => {

                                        let follows_clean = [];

                                        follows.forEach( (follow) => {

                                            follows_clean.push(follow.followed)

                                        })

                                        return follows_clean

                                }).catch((err) => {

                                    return handleError(err);

                                });

    let followed = await Follow.find({ "followed": userId }).select({ '_id': 0, '__v': 0, 'followed': 0 }).exec().then( (follows) => {

                                        let follows_clean = [];

                                        follows.forEach( (follow) => {

                                            follows_clean.push(follow.user)

                                        })

                                        return follows_clean

                                }).catch((err) => {

                                    return handleError(err);

                                });

    return {
        following: following,
        followed: followed
    }

}

// - ******************************************************************************** - //

function getFollowedUsers( req, res ) {

    let userId = req.user.sub;

    if( req.params.id && req.params.page ){

        userId = req.params.id;

    }

    let page = 1;

    if( req.params.page ) {

        page = req.params.page;

    } else {

        page = req.params.id;

    }

    let itemsPerPage = 4;

    Follow.find( { followed: userId }).populate( 'user' ).paginate(page, itemsPerPage, (err, follows, total) => {

        if( err ) return res.status(500).send({ message: 'There was an error on the following list.' });

        if( !follows ) return res.status(404).send({ message: 'You have not followers.' });
        
        followUserIds( userId ).then( (value) => {

            return res.status(200).send({
                total: total,
                pages: Math.ceil(total/itemsPerPage),
                follows,
                users_following: value.following,
                users_follow_me: value.followed,
            })

        })

    });

}

// - ******************************************************************************** - //
// *  RETRIEVES USERS THAT THE CLIENT FOLLOWS
function getMyFollows( req, res ){

    let userId = req.user.sub;

    // let followed = req.params.followed;

    let find = Follow.find({ user: userId });

    if ( req.params.followed ) {

        find = Follow.find({ followed: userId });

    }
    
    find.populate('user followed').exec((err, follows) => {

        if( err ) return res.status(500).send({ message: 'There was an error on the following list.' });

        if( !follows ) return res.status(404).send({ message: 'You are not following any user.' });

        return res.status(200).send({ follows });

    })
}

// - ******************************************************************************** - //


// - ******************************************************************************** - //

module.exports = {
    prueba,
    saveFollow,
    deleteFollow,
    getFollowingUser,
    getFollowedUsers,
    getMyFollows,
}