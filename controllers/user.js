'use strict';

// * LIBRARYS
// * BCRYPT LIBRARY
const bcrypt = require('bcrypt')
// * HOW MANY TIMES IT ENCRYPTS
const saltRounds = 10
// * MONGOOSE PAGINATION
require ('mongoose-pagination');
// * FILE SYSTEM
const fs = require('fs');
// * PATH
const path = require('path');


// * SERVICES
const jwt = require('../services/jwt');

// * MODELS
const User = require('../models/user');
const Follow = require('../models/follow');
const Publication = require('../models/publication');



// | *********************************************************** | //

// * TEST METHODS
function home (req, res) {
    res.status(200).send({
        message: 'Hola Mundo desde el servidor de NodeJS'
    });
};

function pruebas(req, res) {
    console.log(req.body);

    res.status(200).send({
        message: 'Acción de pruebas en el servidor de NodeJS'
    })
}

// | *********************************************************** | //


// * SAVE USER
function saveUser(req, res){
    let params = req.body

    let user = new User()

    if(params.name && params.surname &&
       params.nick && params.email &&
       params.password) {

        user.name = params.name
        user.surname = params.surname
        user.nick = params.nick
        user.email = params.email
        user.role = 'ROLE_USER'
        user.image = null

        User.find({ $or: [ 
                {email: user.email.toLowerCase()},
                {nick: user.nick.toLowerCase()}
             ]}).exec( (err, users) => {
                if( err ) return res.status(200).send({ message: 'Error en la petición de usuarios' })

                if ( users && users.length >= 1 ){
                    return res.status(200).send({ message: 'El usuario que intentas registrar ya existe.'})
                } else {
                    // * CIPHER PASSWORD AND SAVE DATA
                    bcrypt.hash(params.password, saltRounds, function(err, hash){
                        user.password = hash

                        user.save((err, userStored) => {
                            if(err) return res.status(500).send({ message: 'Error al guardar el usuario.' })
                
                            if(userStored){ 
                                res.status(200).send({ user: userStored }) 
                            } else { 
                                res.status(404).send({ message: 'No se ha registrado el usuario.' }) 
                            }
                        })
                    })
                }
             })

    } else {
        res.status(200).send({
            message: 'Envia todos los campos necesarios'
        })
    }
}

// | *********************************************************** | //

// * LOGIN USER
function loginUser(req, res) {
    const params = req.body;

    let email = params.email;
    let password = params.password;
    let token = params.gettoken;

    User.findOne( {email:email}, (err, user) => {

        if(err){
            res.status(500).send({
                message: 'There was a problem on the request.'
            })
        };

        if(user){
            // * COMPARE BOTH PASSWORDS
            bcrypt.compare( password, user.password, (err, check) => {
                if (check){

                    // * CHECK FOR THE TOKEN
                    if(token){
                        // * RETURN AND GENERATE TOKEN
                        return res.status(200).send({
                            token: jwt.createToken(user)
                        });

                    } else {
                        // * AVOID SHOWING THE PASSWORD TURNING IT TO undefined
                        user.password = undefined;

                        // * RETURN USER'S DATA
                        return res.status(200).send({
                            user: user
                        })
                    }

                    
                } else {
                    res.status(404).send({ message: "User cannot be identified" })
                }
            });
        } else {
            res.status(404).send({ message: "User does not exist." })
        }
    } )
}

// | *********************************************************** | //

// * GETS USER
function getUser(req, res){
    
    let userId = req.params.id;

    User.findById( userId, (err, user) => { 

        if(err) return res.status(500).send({ message: 'There was an error on the user request .' })

        if(!user) return res.status(404).send({ message: 'User does not exist.' })

        followThisUser( req.user.sub,  userId).then( (value) => {

            // * HIDE CLIENT PASSWORD
            user.password = undefined;

            return res.status(200).send({ 
                user, 
                following: value.following,
                followed: value.followed
            })
            
        })

     })

}

async function followThisUser( identity_user_id, user_id) {


    // * VERIFY IF THE CLIENT FOLLOWS AN SPECIFIC USER
    let following =  await Follow.findOne({ "user" : identity_user_id, "followed": user_id }).exec().then( (follow) => {

                                return follow;

                            }).catch( (err) => {

                                return handleError(err);
                            }) 

    // * VERIFY IF AN SPECIFIC USER FOLLOWS THE CLIENT
    let followed = await Follow.findOne({ "user" : user_id, "followed": identity_user_id }).exec().then( (follow) => {

                            return follow;

                         }).catch( (err) => {

                            return handleError( err );

                         })


    return {
        following: following,
        followed: followed
    }
}

// | *********************************************************** | //

// * GET USER'S LIST
function getUsers(req, res){
    // * VERIFY IDENTITY FROM THE TOKEN
    let identify_user_id = req.user.sub

    let page = 1

    // * VALIDATE PAGE TO SHOW
    if (req.params.page) {
        page = req.params.page
    }

    let itemsPerPage = 5

    User.find()
        .sort('_id')
        .paginate(page, itemsPerPage, (err, users, total) => {

            if(err) return res.status(500).send({ message: 'There was an error on the request.' })

            if(!users) return res.status(404).send({ message: 'There are not users available.'  })

            followUserIds( identify_user_id ).then( (value) => {

                return res.status(200).send({
                    users,
                    users_following: value.following,
                    users_follow_me: value.followed,
                    total,
                    pages: Math.ceil(total/itemsPerPage)
                })

            })

            

        })

}

async function followUserIds(userId) {

    let following = await Follow.find({ "user": userId }).select({ '_id': 0, '__v': 0, 'user': 0 }).exec().then( (follows) => {

                                        let follows_clean = [];

                                        follows.forEach( (follow) => {

                                            follows_clean.push(follow.followed)

                                        })

                                        return follows_clean

                                }).catch((err) => {

                                    return handleError(err);

                                });

    let followed = await Follow.find({ "followed": userId }).select({ '_id': 0, '__v': 0, 'followed': 0 }).exec().then( (follows) => {

                                        let follows_clean = [];

                                        follows.forEach( (follow) => {

                                            follows_clean.push(follow.user)

                                        })

                                        return follows_clean

                                }).catch((err) => {

                                    return handleError(err);

                                });

    return {
        following: following,
        followed: followed
    }

}

// | *********************************************************** | //

function getCounters (req, res) {

    // * GET CLIENT'S ID
    let userId = req.user.sub;

    // * CLIENT'S ID BY THE URL
    if( req.params.id ) {

        userId = req.params.id;

    }

    getCountFollow( userId ).then( (value) => {

        return res.status(200).send( value );

    })

}

async function getCountFollow(user_id) {

    let following = await Follow.countDocuments(( { "user" : user_id } )).exec().then( (count) => {

                                        return count;

                                    }).catch( (err) => {

                                        return handleError(err);

                                    })

    let followed = await Follow.countDocuments(( { "followed" : user_id } )).exec().then( (count) => {

                                        return count;

                                    }).catch( (err) => {

                                        return handleError(err);

                                    })
    
    let publications = await Publication.countDocuments(( { "user": user_id } )).exec().then( (count) => {

                                        return count;

                                    }).catch( (err) => {

                                        return handleError(err);
                                        
                                    })

    return {
        following: following,
        followed: followed,
        publications: publications
    }

}

// | *********************************************************** | //

// * UPDATE USER DATA
function updateUser(req, res){

    let userId = req.params.id;

    let update = req.body;


    // * DELETE PASSWORD
    delete update.password

    if ( userId != req.user.sub ){

        return res.status(500).send({ message: 'There was an error on the request.' })

    }

    User.find({ $or: [ 
        {email: update.email.toLowerCase()},
        {nick: update.nick.toLowerCase()}
     ]}).exec( (err, users) => {

        let user_isset = false;

        users.forEach( (user) =>  {

            if ( user && user._id != userId ) user_isset = true;

        });

        if ( user_isset ) return res.status(404).send({ message: 'Los datos ya están en uso.' });

        User.findByIdAndUpdate(userId, update, {new: true}, (err, userUpdated) => {


            if(err) return res.status(500).send({ message: 'There was an error on the request.' })
    
            if(!userUpdated) return res.status(404).send({ message: 'There is not user to update.' })
    
            return res.status(200).send({ user: userUpdated });
    
            
        })

     })

}

// | *********************************************************** | //

// * UPLOAD AN AVATAR IMAGE
function uploadImage(req, res) {

    const userId = req.params.id

    console.log(req.files);

    const size = req.files.image.size
    const type = req.files.image.type
    const file_path = req.files.image.path;

    console.log(size)

    if ( size > 0) {

        const file_split = file_path.split('\/');

        const file_name = file_split[2]

        const ext_split = file_name.split('\.')

        const file_ext = ext_split[1];

        if( userId != req.user.sub ){

            return removeFilesOfUploads(res, file_path, 'Does not have permission to update user`s data')
    
        }

        if ( file_ext === 'png' || file_ext === 'jpg' || file_ext === 'jpeg' || file_ext === 'gif' ) {

            User.findByIdAndUpdate(userId, {image: file_name}, {new: true}, (err, userUpdated) => {

                if(err) return res.status(500).send({ message: 'There was an error on the request.' })

                if(!userUpdated) return res.status(404).send({ message: 'There is not user to update.' })

                return res.status(200).send({ user: userUpdated });

            })
            
        } else {

            return removeFilesOfUploads(res, file_path, 'Extention is invalid')

        }


    } else {

        return removeFilesOfUploads(res, file_path, 'The file was not uploaded')

    }

}

// | *********************************************************** | //

function removeFilesOfUploads(res, file_path, message) {
    fs.unlink(file_path, (err) => {
        return res.status(404).send({message: message});
    });
}
    


// | *********************************************************** | //

function getImageFile ( req, res ) {

    const image_file = req.params.imageFile

    const path_file = `./uploads/users/${image_file}`

    fs.exists(path_file, (exists) => {

        if(exists) {

            res.sendFile(path.resolve(path_file));

        } else {

            res.status(200).send({ message: 'Image does not exists.'})

        }


    })

}

// | *********************************************************** | //

module.exports = {
    home,
    pruebas,
    saveUser,
    loginUser,
    getUser,
    getUsers,
    getCounters,
    updateUser,
    uploadImage,
    getImageFile,
}